#!/bin/bash
# RUN COMMAND FROM PROJECT ROOT DIRECTORY

# Exit when any command fails
set -e

rm -rf build

# Configure
cmake -B build -G "Ninja Multi-Config" \
  -DCMAKE_TOOLCHAIN_FILE="cmake/generic-gcc-avr.cmake" -S .

# Build
cmake --build build --config Release # --verbose

tree build

