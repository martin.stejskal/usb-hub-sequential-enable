#!/bin/bash
# RUN COMMAND FROM PROJECT ROOT DIRECTORY

# Exit when any command fails
set -e

./build.sh

# Flash
cmake --build build/ -t upload_USBhubSeqEnOutputs # --verbose
