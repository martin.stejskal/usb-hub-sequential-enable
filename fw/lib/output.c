/**
 * @file
 * @author Martin Stejskal
 * @brief Simple output driver
 */
// ===============================| Includes |================================
#include "output.h"
// ================================| Defines |================================
#include "config.h"
#include "gpio.h"
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Returns pin number for selected output
 *
 * @param e_output Selected output
 * @return Pin number used by GPIO library
 */
static te_gpio_pin _get_pin(const te_output_select e_output);

// =========================| High level functions |==========================
void output_init(void) {
  // Set all GPIO to output, but keep them off
  const te_gpio_cfg s_cfg = {
      .e_dir = GPIO_DIR_OUT,
      .out.b_out_value = 1,  // Logic is inverted. Active is low level
  };

  for (te_output_select e_output = (te_output_select)0; e_output < OUT_ALL;
       e_output++) {
    gpio_configure_pin(_get_pin(e_output), s_cfg);
  }
}

void output_set(const te_output_select e_output, const bool b_value) {
  // Note that logic is inverted - enable by logic low due to HW -> that is why
  // it is inverted in code later on

  if (e_output > OUT_ALL) {
    // Invalid input argument - do nothing
    return;

  } else if (e_output == OUT_ALL) {
    // Enable output by output
    for (te_output_select e_current_output = (te_output_select)0;
         e_current_output < OUT_ALL; e_current_output++) {
      gpio_set_pin(_get_pin(e_current_output), !b_value);
    }

  } else {
    // Set only 1 output
    gpio_set_pin(_get_pin(e_output), !b_value);
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static te_gpio_pin _get_pin(const te_output_select e_output) {
  switch (e_output) {
    case OUT_0:
      return IO_OUT_0;

    case OUT_1:
      return IO_OUT_1;

    case OUT_2:
      return IO_OUT_2;

    case OUT_3:
      return IO_OUT_3;

    case OUT_4:
      return IO_OUT_4;

    case OUT_5:
      return IO_OUT_5;

    case OUT_6:
      return IO_OUT_6;

    case OUT_7:
      return IO_OUT_7;

    default:
      // Should not happen. But as fail safe, let's mess with output 0
      return IO_OUT_0;
  }
}
