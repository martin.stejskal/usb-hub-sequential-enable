/**
 * @file
 * @author Martin Stejskal
 * @brief Simple driver for LEDs
 */
#ifndef __LED_H__
#define __LED_H__
// ===============================| Includes |================================
#include <stdbool.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  LED_0 = 0,
  LED_1,
  LED_2,
  LED_3,
  LED_4,
  LED_5,
  LED_6,
  LED_7,
  LED_ALL,
} te_led_select;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
void led_init(void);

void led_set(const te_led_select e_led, const bool b_value);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __LED_H__
