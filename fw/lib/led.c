/**
 * @file
 * @author Martin Stejskal
 * @brief Simple driver for LEDs
 */
// ===============================| Includes |================================
#include "led.h"

#include "config.h"
#include "gpio.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
/**
 * @brief Returns pin number for selected LED
 *
 * @param e_led Selected LED
 * @return Pin number for GPIO library
 */
static te_gpio_pin _get_pin(const te_led_select e_led);

// =========================| High level functions |==========================
void led_init(void) {
  // Set all GPIO to output, but keep them off
  const te_gpio_cfg s_cfg = {
      .e_dir = GPIO_DIR_OUT,
      .out.b_out_value = 0,
  };

  for (te_led_select e_led = (te_led_select)0; e_led < LED_ALL; e_led++) {
    gpio_configure_pin(_get_pin(e_led), s_cfg);
  }
}

void led_set(const te_led_select e_led, const bool b_value) {
  if (e_led > LED_ALL) {
    // Invalid input argument - do nothing
    return;

  } else if (e_led == LED_ALL) {
    // Enable LED by LED
    for (te_led_select e_current_led = (te_led_select)0;
         e_current_led < LED_ALL; e_current_led++) {
      gpio_set_pin(_get_pin(e_current_led), b_value);
    }

  } else {
    // Only 1 LED is required
    gpio_set_pin(_get_pin(e_led), b_value);
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static te_gpio_pin _get_pin(const te_led_select e_led) {
  switch (e_led) {
    case LED_0:
      return IO_LED_0;

    case LED_1:
      return IO_LED_1;

    case LED_2:
      return IO_LED_2;

    case LED_3:
      return IO_LED_3;

    case LED_4:
      return IO_LED_4;

    case LED_5:
      return IO_LED_5;

    case LED_6:
      return IO_LED_6;

    case LED_7:
      return IO_LED_7;

    default:
      // Should not happen. But as fail safe, let's mess with LED 0
      return IO_LED_0;
  }
}
