/**
 * @file
 * @author Martin Stejskal
 * @brief Simple output driver
 */
#ifndef __OUTPUT_H__
#define __OUTPUT_H__
// ===============================| Includes |================================
#include <stdbool.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef enum {
  OUT_0 = 0,
  OUT_1,
  OUT_2,
  OUT_3,
  OUT_4,
  OUT_5,
  OUT_6,
  OUT_7,
  OUT_ALL,
} te_output_select;
// ===========================| Global variables |============================

// ===============================| Functions |===============================

// =========================| High level functions |==========================
void output_init(void);

void output_set(const te_output_select e_output, const bool b_value);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __OUTPUT_H__
