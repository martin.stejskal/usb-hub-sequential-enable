/**
 * @file
 * @author Martin Stejskal
 * @brief Main file for PWM 4 Car light project
 */
// ===============================| Includes |================================
#include <avr/sleep.h>
#include <stdio.h>
#include <util/delay.h>

#include "config.h"
#include "gpio.h"
#include "led.h"
#include "output.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================
// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
static void _sleep(void);
// ================| Internal function prototypes: low level |================
static void _delay_power_on(void);
static void _delay_enable_sequence(void);
static void _delay_fade_off_led(void);
// =========================| High level functions |==========================
int main(void) {
  output_init();
  led_init();

  // Settle all outputs
  _delay_power_on();

  // Enable all outputs sequentially with LEDs
  for (uint8_t u8_seq_number = 0; u8_seq_number < OUT_ALL; u8_seq_number++) {
    led_set((te_led_select)u8_seq_number, 1);
    output_set((te_output_select)u8_seq_number, 1);
    _delay_enable_sequence();
  }

  // Turn off all LEDs
  for (te_led_select e_led = (te_led_select)0; e_led < LED_ALL; e_led++) {
    led_set(e_led, 0);
    _delay_fade_off_led();
  }

  // Put CPU into sleep mode. CPU should not wake up
  _sleep();

  return 0;
}
// ========================| Middle level functions |=========================

// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static void _delay_power_on(void) { _delay_ms(POWER_ON_DELAY); }
static void _delay_enable_sequence(void) { _delay_ms(ENABLE_DELAY_MS); }
static void _delay_fade_off_led(void) { _delay_ms(LED_FADE_OFF_DELAY); }

static void _sleep(void) {
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  sleep_cpu();
}
