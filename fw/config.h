/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file
 *
 * Mainly hardware related stuff
 */
#ifndef __CONFIG_H__
#define __CONFIG_H__
// ===============================| Includes |================================
#include "gpio.h"
// ================================| Defines |================================
/**
 * @brief Hardware related stuff
 *
 * @{
 */
#define IO_OUT_0 (PD_2)
#define IO_OUT_1 (PD_3)
#define IO_OUT_2 (PD_4)
#define IO_OUT_3 (PB_7)
#define IO_OUT_4 (PD_5)
#define IO_OUT_5 (PD_6)
#define IO_OUT_6 (PD_7)
#define IO_OUT_7 (PB_0)

#define IO_LED_0 (PC_2)
#define IO_LED_1 (PC_1)
#define IO_LED_2 (PC_0)
#define IO_LED_3 (PB_5)
#define IO_LED_4 (PB_4)
#define IO_LED_5 (PB_3)
#define IO_LED_6 (PB_2)
#define IO_LED_7 (PB_1)

/**
 * @}
 */

/**
 * @brief Power on delay
 *
 * Since MOS-FET are enabled by low, at very initial state they might be
 * power USB devices for a moment. Therefore this delay ensures that device
 * will power off and when MOS-FET will be enabled by MCU, it will enable
 * device properly.
 */
#define POWER_ON_DELAY (2000)

/**
 * @brief Delay between initialization of USB ports
 */
#define ENABLE_DELAY_MS (500)

/**
 * @brief Fade off delay for all LED
 *
 * When initialization of all ports is finished, LEDs will be turned off. This
 * delay define how fast they will be powered off onve after another.
 *
 */
#define LED_FADE_OFF_DELAY (250)
// ==========================| Preprocessor checks |==========================

#endif  // __CONFIG_H__
