# About
 * Goal is to initialize devices in predefined order so they will pop up at
   system in more predicable order.
 * This hack turn on sequentially USB ports at USB hub.
 * You need to take some old USB hub and hack it. But it is nothing really
   complicated. Simply cut power supply for every single USB connector and
   wire it via MOS-FET.
 * If you want to have fancy effect, you can add LEDs ;)

![demo](doc/demo.gif)

# Download HEX
 * HEX files are automatically build :) You can download latest below
 * [CI job artifact](https://gitlab.com/martin.stejskal/usb-hub-sequential-enable/-/jobs/artifacts/master/download?job=build_master)

# Schema
 * [PDF](hw/export/USB_SEQ_EN-00_sch.pdf)
 
 ![schema](hw/export/USB_SEQ_EN-00_sch.png)
